<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Dreamhouse Classic App</description>
    <formFactors>Large</formFactors>
    <label>Dreamhouse Classic</label>
    <tab>standard-Chatter</tab>
    <tab>standard-Account</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-report</tab>
    <tab>Suggestion__c</tab>
    <tab>Battle_Station__c</tab>
    <tab>Cat__c</tab>
    <tab>Interested_Person__c</tab>
</CustomApplication>
