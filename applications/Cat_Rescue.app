<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#0070D2</headerColor>
    </brand>
    <description>Track cats and cat adopters.</description>
    <formFactors>Large</formFactors>
    <label>Cat Rescue</label>
    <navType>Standard</navType>
    <tab>Cat__c</tab>
    <tab>Interested_Person__c</tab>
    <uiType>Lightning</uiType>
    <utilityBar>Cat_Rescue_UtilityBar</utilityBar>
</CustomApplication>
