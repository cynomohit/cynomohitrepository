<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <formFactors>Large</formFactors>
    <navType>Console</navType>
    <tab>standard-home</tab>
    <tab>standard-Lead</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-Campaign</tab>
    <tab>standard-Task</tab>
    <tab>standard-Event</tab>
    <tab>standard-ContentNote</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Feed</tab>
    <tab>standard-CollaborationGroup</tab>
    <tab>standard-File</tab>
    <tab>Suggestion__c</tab>
    <tab>Battle_Station__c</tab>
    <tab>Cat__c</tab>
    <tab>Interested_Person__c</tab>
    <uiType>Lightning</uiType>
    <utilityBar>LightningSalesConsole_UtilityBar</utilityBar>
</CustomApplication>
