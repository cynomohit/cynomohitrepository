({
    myAction : function(component, event, helper) {
        alert('recType-- '+component.get('recType'));
    },
    
    createRecord : function (component, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Opportunity"
        });
        createRecordEvent.fire();
    }
    
})