<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Opportunity_Stage_To_Negotiation</fullName>
        <description>Update Opportunity Stage To Negotiation Rejected</description>
        <field>StageName</field>
        <literalValue>Negotiation Rejected</literalValue>
        <name>Update Opportunity Stage To Negotiation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Stage_To_Scheduled</fullName>
        <description>Update Opportunity Stage To Scheduled</description>
        <field>StageName</field>
        <literalValue>Scheduled</literalValue>
        <name>Update Opportunity Stage To Scheduled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
