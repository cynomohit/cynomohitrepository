<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ContractPod_CMS__Expire_Contract</fullName>
        <field>Status</field>
        <literalValue>Expired Contract</literalValue>
        <name>Expire Contract</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ContractPod_CMS__Fire_Checkin_Trigger</fullName>
        <field>ContractPod_CMS__Check_In_Trigger__c</field>
        <literalValue>1</literalValue>
        <name>Fire Checkin Trigger</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ContractPod_CMS__Fire_Trigger</fullName>
        <field>ContractPod_CMS__Get_Signed_Document__c</field>
        <literalValue>1</literalValue>
        <name>Fire Trigger</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
