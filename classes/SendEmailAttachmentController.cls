public class SendEmailAttachmentController {
    
    // Pass in the URL for the request 
    
    // For the purposes of this sample,assume that the URL 
    
    // returns the XML shown above in the response body 
    
    
    public  void parseResponseDom()
    {
        String url = 'https://app.contractpod.com/Zappistore/Uploads/Zappistore/Contract Documents/PDF/Audible-UK_Terms-and-Conditions_Create-New-Contract_Version_1_8d729a8d-c19c-4c77-a1ad-fefb3942765f.pdf';
        
       /* Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setBody(JSON.serialize(url));
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/pdf');
        req.setTimeout(120000);      
        
        HttpResponse res = h.send(req);
        system.debug('---Body---'+res.getBody());
        system.debug('---Body2---'+res.getBodyAsBlob());*/
        
         Http h = new Http(); 
        HttpRequest req = new HttpRequest(); 
        //Replace any spaces in url with %20 
        url = url.replace(' ', '%20'); 
        //Set the end point URL
        req.setEndpoint(url); 
        req.setMethod('GET'); 
        req.setHeader('Content-Type', 'application/pdf'); 
        req.setCompressed(true); 
        req.setTimeout(60000); 
        //Now Send HTTP Request
        HttpResponse res  = h.send(req); 
        system.debug('Response from Server: ' + res.getBody()); 
        //getBodyAsBlob method was will convert the response into Blob 
        blob retFile = res.getBodyAsBlob();
        system.debug('----retFile--'+retFile);
        
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        // Set recipients to two contact IDs.
        // Replace IDs with valid record IDs in your org.
        message.toAddresses = new String[] { 'nitin.dangwal@cynoteck.com', 'mohit.gulati@cynoteck.com' };
        message.optOutPolicy = 'FILTER';
        message.subject = 'Contract Document';
        message.plainTextBody = 'Please find the attached contract document.';
        
        Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
        attach.setContentType('application/pdf');
        attach.setFileName('testDoc.pdf');
        attach.setInline(false);
        attach.Body = retFile;
        
        Messaging.EmailFileAttachment attach_word = new Messaging.EmailFileAttachment();
        attach_word.setContentType('application/msword');
        attach_word.setFileName('testDoc.doc');
        attach_word.setInline(false);
        attach_word.Body = retFile;
        
        message.setFileAttachments(new Messaging.EmailFileAttachment[] {attach,attach_word}); 
        
        Messaging.SingleEmailMessage[] messages = 
            new List<Messaging.SingleEmailMessage> {message};
                 Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: '
                  + results[0].errors[0].message);
        }
    }
    
}