public with sharing class Sample {
     public Opportunity opp{get;set;}
     public boolean showSection{get;set;}
     public boolean showSection1{get;set;}
     public boolean showSection2{get;set;}
     public string label{get;set;}
     public boolean show{get;set;}
     
   private ApexPages.StandardController sc;
  
    public Sample(ApexPages.StandardController controller) {
    
           showSection = false;
           showSection1 = false;
           showSection2 = false;
          sc = controller;
          opp = (opportunity)controller.getRecord(); 
          system.debug('----Recalled---');
          system.debug('----Recalled2---');
      
    }
    
    public pagereference proceed(){ 

   
     system.debug('---opp-'+opp);
     system.debug('---opp-'+opp.Show_Other_Details__c);
     
     system.debug('---showSection -'+showSection);
         return null;
    }
    
    public PageReference save()
    { 
        opp = (opportunity)sc.getRecord();
        system.debug('---opp-'+opp);
        system.debug('---opp-'+opp.Description);
        sc.save();
    
         return null;
    }
    
   
    Public pagereference checkBoxValue()
    {
        system.debug('----show---'+show);
        system.debug('----label---'+label);
        if(label == 'Show Other Details')
        {
            showSection = true;
        }
        
        
        else if(label == 'Fill Details Now')
        {
            showSection1 = true;
        }
        
        return null;
    }
    
   
}