<?xml version="1.0" encoding="UTF-8"?>
<Profile xmlns="http://soap.sforce.com/2006/04/metadata">
    <applicationVisibilities>
        <application>Battle_Station_Construction</application>
        <default>false</default>
        <visible>false</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>Cat_Rescue</application>
        <default>false</default>
        <visible>false</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>Dreamhouse_Classic</application>
        <default>false</default>
        <visible>true</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>Relaxation_Gauntlet</application>
        <default>false</default>
        <visible>true</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>Suggestion_Box</application>
        <default>false</default>
        <visible>false</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>standard__AppLauncher</application>
        <default>false</default>
        <visible>false</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>standard__Chatter</application>
        <default>false</default>
        <visible>false</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>standard__Community</application>
        <default>false</default>
        <visible>false</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>standard__Content</application>
        <default>false</default>
        <visible>false</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>standard__LightningInstrumentation</application>
        <default>false</default>
        <visible>false</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>standard__LightningSales</application>
        <default>false</default>
        <visible>false</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>standard__LightningSalesConsole</application>
        <default>false</default>
        <visible>false</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>standard__LightningService</application>
        <default>false</default>
        <visible>false</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>standard__Marketing</application>
        <default>false</default>
        <visible>false</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>standard__Platform</application>
        <default>true</default>
        <visible>true</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>standard__Sales</application>
        <default>false</default>
        <visible>false</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>standard__Service</application>
        <default>false</default>
        <visible>false</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>standard__ServiceConsole</application>
        <default>false</default>
        <visible>false</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>standard__Sites</application>
        <default>false</default>
        <visible>false</visible>
    </applicationVisibilities>
    <classAccesses>
        <apexClass>Capstorm_Restore</apexClass>
        <enabled>false</enabled>
    </classAccesses>
    <classAccesses>
        <apexClass>CaseManager</apexClass>
        <enabled>false</enabled>
    </classAccesses>
    <classAccesses>
        <apexClass>CheckRecordType</apexClass>
        <enabled>false</enabled>
    </classAccesses>
    <classAccesses>
        <apexClass>PaginationClass</apexClass>
        <enabled>false</enabled>
    </classAccesses>
    <classAccesses>
        <apexClass>Sample</apexClass>
        <enabled>false</enabled>
    </classAccesses>
    <classAccesses>
        <apexClass>SendEmailAttachmentController</apexClass>
        <enabled>false</enabled>
    </classAccesses>
    <classAccesses>
        <apexClass>StringArrayTest</apexClass>
        <enabled>false</enabled>
    </classAccesses>
    <classAccesses>
        <apexClass>myEmailService</apexClass>
        <enabled>false</enabled>
    </classAccesses>
    <classAccesses>
        <apexClass>opportunityListCon</apexClass>
        <enabled>false</enabled>
    </classAccesses>
    <custom>false</custom>
    <fieldPermissions>
        <editable>true</editable>
        <field>Cat__c.Cat_Breed__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Energy_Audit__c.Audit_Notes__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Energy_Audit__c.Type_of_Installation__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.Address__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.AdventurePackageId__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.Adventure_Package_Cost__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.Adventure__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.Deposit__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.Dietary_Restrictions__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.Email__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.Expedition_Leader__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.Explorer__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.Home_Phone__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.Is_Exempt__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.Lead_Marketer__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.Mobile__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.Opportunity__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.Preferred_form_of_Contact__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.Schedule_Date__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.Special_Needs__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.Status__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Fulfillment__c.Submitted_Waiver__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Interested_Person__c.Cat_Breed__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>false</editable>
        <field>Knowledge__kav.Answer__c</field>
        <readable>false</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>false</editable>
        <field>Knowledge__kav.Question__c</field>
        <readable>false</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Order__c.Address__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Order__c.Contact_Number__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Order__c.Email__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Suggestion__c.Implemented_Date__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>false</editable>
        <field>Suggestion__c.Number_of_Days_Open__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Suggestion__c.Status__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Suggestion__c.Suggestion_Category__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Suggestion__c.Suggestion_Description__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <layoutAssignments>
        <layout>Account-Account Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Asset-Asset Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>AssetRelationship-Asset Relationship Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Battle_Station__c-Battle Station Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Campaign-Campaign Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CampaignMember-Campaign Member Page Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Case Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CaseClose-Close Case Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Cat__c-Cat Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CommunityMemberLayout-Community Member Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Contact-Contact Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ContentVersion-Content Version Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Contract-Contract Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>DandBCompany-D%26B Company Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>DuplicateRecordSet-Duplicate Record Set Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>EmailMessage-Email Message Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Energy_Audit__c-Energy Audit Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Event-Event Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>FeedItem-Feed Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Fulfillment__c-Fulfillment Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Global-Global Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Goal-Goal Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>GoalLink-Goal Link Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Idea-Idea Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Interested_Person__c-Interested Person Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>JobTracker-Job Tracker Layout - Winter %2716</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Knowledge__kav-Knowledge Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Knowledge__kav-Knowledge Layout</layout>
        <recordType>Knowledge__kav.FAQ</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Lead-Lead Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>LiveAgentSession-Live Agent Session Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>LiveChatTranscript-Live Chat Transcript Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>LiveChatTranscriptActive-Live Chat Transcript %28In Progress%29 Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>LiveChatTranscriptEvent-Live Chat Transcript Event Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>LiveChatTranscriptWaiting-Live Chat Transcript %28Waiting%29 Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>LiveChatVisitor-Live Chat Visitor Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Macro-Macro Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Metric-Metric Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>MetricDataLink-Metric Data Link Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Opportunity-Opportunity Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OpportunityLineItem-Opportunity Product Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Order-Order Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OrderItem-Order Product Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Order__c-Order Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Pricebook2-Price Book Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>PricebookEntry-Price Book Entry Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Product2-Product Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>QuickText-Quick Text Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Scorecard-Scorecard Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ScorecardAssociation-Scorecard Association Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ScorecardMetric-Scorecard Metric Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SocialPersona-Social Persona Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SocialPost-Social Post Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Solution-Solution Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Suggestion__c-Suggestion Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Task-Task Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>User-User Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserAlt-User Profile Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserProvAccount-User Provisioning Account Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserProvisioningLog-User Provisioning Log Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserProvisioningRequest-User Provisioning Request Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkCoaching-Coaching Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkFeedback-Feedback Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkFeedbackQuestion-Feedback Question Layout - Winter %2716</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkFeedbackQuestionSet-Feedback Question Set Layout - Winter %2716</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkFeedbackRequest-Feedback Request Layout - Winter %2716</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkFeedbackTemplate-Feedback Template Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkPerformanceCycle-Performance Cycle Layout - Winter %2716</layout>
    </layoutAssignments>
    <pageAccesses>
        <apexPage>FieldSet_VFPage</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>Opportunity_List</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>PaginationPage</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>PowerBI_Dashboard</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <recordTypeVisibilities>
        <default>false</default>
        <recordType>Knowledge__kav.FAQ</recordType>
        <visible>false</visible>
    </recordTypeVisibilities>
    <tabVisibilities>
        <tab>Battle_Station__c</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Cat__c</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Energy_Audit__c</tab>
        <visibility>DefaultOff</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Fulfillment__c</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Interested_Person__c</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Knowledge__kav</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Suggestion__c</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <userLicense>Partner App Subscription</userLicense>
    <userPermissions>
        <enabled>true</enabled>
        <name>ActivitiesAccess</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>AddDirectMessageMembers</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>AllowUniversalSearch</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>AllowViewKnowledge</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ApexRestServices</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ApiEnabled</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>AssignTopics</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterEditOwnPost</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterFileLink</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterInternalUser</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterInviteExternalUsers</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterOwnGroups</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ContentWorkspaces</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>CreateCustomizeFilters</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>CreateTopics</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>CustomMobileAppsAccess</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>DistributeFromPersWksp</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EditEvent</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EditTask</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EditTopics</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EmailMass</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EmailSingle</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EnableCommunityAppLauncher</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EnableNotifications</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ExportReport</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ImportPersonal</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>LightningExperienceUser</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>RemoveDirectMessageMembers</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>RunReports</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>SelectFilesFromSalesforce</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ShowCompanyNameAsUserBadge</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>SubscribeToLightningDashboards</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>SubscribeToLightningReports</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>UseWebLink</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewHelpLink</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewRoles</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewSetup</name>
    </userPermissions>
</Profile>
